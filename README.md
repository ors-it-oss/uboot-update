update-uboot
============

Script for updating U-boot's environment and boot.scr, similar to update-grub

# Overview
update-uboot will compile directories of files into bootscripts and environments for [Das U-Boot](http://www.denx.de/wiki/U-Boot).
It performs os detection on the block devices on the system and make boot entries for those as well.

## Warning
I test & run this script and save myself a lot of hassle on my [Compulab Utilite](http://utilite-computer.com/), running Das U-Boot 2014.04.
That also means that while I took into account a lot of potential variables for different boards, things are bound to break on something else.

Please let me know what happens, I'd love to see some feedback ;)

This script will attempt to overwrite large parts of U-Boot's environment.
If you don't feel comfortable with U-Boot's environment and/or don't have easy access to it's serial line or powerfeed, I wouldn't use this just yet.

Having said that, I feel a script like this to be instrumental to U-Boot powered platforms and their smooth integration in distro's ecosystems.
And if you're trying to cook up your own kernels & DTB's like I'm doing way too much, it's a very handy-dandy tool.

## What it doesn't do (but should)
* do something with multi-images (U-Boot images containing RAMdisks and/or DTBs)
* autodetect the necessity for a separate DTB
* different bootargs for different images/OS'es
* run on anything other than Linux  

# Usage
In $CONF_DIR (/etc/u-boot) you'll find a bunch of stuff.
The script needs:
* `os-prober`
* `lsblk`
* U-Boot's `mkimage`, `fw_setenv` & `fw_printenv` utils
* root rights

Currently it has no cli args yet.
Upon exection **It will generate & overwrite in /boot or the configured boot**
* `boot.scr`
* `env.scr`
* `uEnv.txt`

## Configuration dir
### .conf files
Will be included by means of configuration options. 
Update.conf contains all vars available.

### .pscr files
Will be compiled into scr images in the configured boot dir, along with env variables to run them.

### boot.d directory
Everything in here will be gathered together as boot.scr

### env.d directory
Everything in here will be gathered together as U-Boot's in-flash environment

### uEnv.d directory
Everything in here will be gathered together as uEnv.txt

## Configuration files

Das U-Boot has three syntaxes, and all three formats can be freely used and combined.
Configuration dirs can contain:

### .cmd files
Interpreted as snippets of Hush shell/scripts

### .penv files
Interpreted as `fw_printenv` and .txt variable lists 

### .senv files
Interpreted in `fw_setenv` syntax

### .tmpl files
Bash includes of which stdout is expected to be in the native format of what we're generating

## Boot environment
**Unless a variable is in the UBOOT_READONLY or UBOOT_RESERVED lists, it will get overwritten/erased!**

For some reason, Utilite doesn't do `fw_setenv` (anymore?). 
Therefore another way of updating the U-Boot env is also available (and default); through the `boot.scr`

If Das U-Boot is already configured to run `boot.scr`, it should automatically insert itself and it's usefuls into the environment.

# Usage

`run stage2` - init everything up to the point where we'd load the `boot.scr`

`uEnv.txt` will, amongst others, contain:
* `dtb[0-9]+`
* `img[0-9]+`
* `scr[0-9]+`
* `os[0-9]+`

All you have to do is string it all together.
Like so:

* `run init from_sata0 load_env` - that's actually what `stage2` does. Init boot devices, set `sata dev 0` as active device, load `uEnv.txt` from it.
* `run img2 dtb_def os1` - boot up the `os0` using `img2` and the default DTB (don't change the run-order)
* `run img_def os_def` - boot up the default OS using the default kernel
* `run scr0` - run script 0 in the (printenv) list
* `run reset_uenv` - Something went awry, want a U-Boot env 'reset'

